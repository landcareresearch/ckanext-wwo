# ckanext-wwo changelog

## 2022-09-02 - Version 0.2.3

- Updated a colorized resource format.

## 2022-09-02 - Version 0.2.2

- Added bing site authentication.

## 2022-09-02 - Version 0.2.1

- Updated colors of file extensions.

## 2022-09-01 - Version 0.2.0

- Changed the color of the file extension tags.

## 2022-08-24 - Version 0.1.65

- Fixed a link in the footer to use _blank instead of blank which was incorrect functionality.

## 2022-08-17 - Version 0.1.64

- Filtered the 'Other' categories for Industry and Misc Facets.

## 2022-08-17 - Version 0.1.63

- Fixed an incorrect email address for informatics support.

## 2022-07-27 - Version 0.1.62

- Sets all search facets are expanded except for land use and formats.

## 2022-07-27 - Version 0.1.61

- Removed maori principles and wellbeings fields.
- Removed overarching principles page.

## 2022-07-26 - Version 0.1.60

- Removed API message in search results.

## 2022-07-26 - Version 0.1.59

- Updated homepage CSS for consistancy.
- Added version to footer.
- Cleaned up footer.

## 2022-07-25 - Version 0.1.58

- Updated footer's license.

## 2022-07-25 - Version 0.1.57

- Changed the header name Data to Datasets.

## 2022-07-25 - Version 0.1.56

- Minor tweaks to Fitness popover text.
- Added Industry field.
- Minor tweak to Resource Meta Data's license spacing.

## 2022-07-25 - Version 0.1.55

- Hide login bar.

## 2022-07-25 - Version 0.1.54

- Updated Fitness popover text.

## 2022-07-22 - Version 0.1.53

- Updated footer with additional text changes.
- Changes to Fitness popover text.

## 2022-07-22 - Version 0.1.52

- Removed an invalid schema element/validator.
- Removed extranious schema related code from the cloned project.
- Added javascript for popover on dataset metadata.

## 2022-07-19 - Version 0.1.51

- Added bread crumb for overarching principles.

## 2022-07-19 - Version 0.1.48, 0.1.49 & 0.1.50

- Fixed grammer issue on privacy page.
- Fixed heading on tos, privacy, and principles.

## 2022-07-19 - Version 0.1.47

- Changed headers for privacy, tos, and principles.
- Changed subheading to match on each page.
- Added last update to the privacy and principles pages.

## 2022-07-15 - Version 0.1.46

- Removed tos/privacy popup from Overarching Principles page.

## 2022-07-15 - Version 0.1.45

- Replaced disclaimer with Overarching Principles.

## 2022-07-14 - Version 0.1.44

- Added information link for all facets
- Changed Privacy and TOS in popup to use open new tabs.

## 2022-07-13 - Version 0.1.43

- Fixed privacy and tos links in TOSP popup.

## 2022-07-13 Version 0.1.42

- Changed case of link in popup.
- Fixed invalid privacy link in popup.
- Made the popup stay in the same place regardless of page scroll.

## 2022-07-12 Version 0.1.41

- Added a privacy popup.
- Added a cookie for checking if the privacy policy and TOS have been accepted.
- Expires the cookie after 30 days according to [GDPR](https://www.jqueryscript.net/other/GDPR-Cookie-Consent-Popup-Plugin.html)

## 2022-07-11 Version 0.1.40

- Added a privacy statement.

## 2022-07-08 Version 0.1.39

- Added link in footer.

## 2022-07-08 Version 0.1.38

- Updated TOS.

## 2022-07-08 Version 0.1.37

- Updated footer.

## 2022-07-07 Version 0.1.36

- Changed spelling of potato in schema.

## 2022-07-06 Version 0.1.35

- Removed Resource title truncation.

## 2022-07-06 Version 0.1.34

- TOS update.

## 2022-07-06 Version 0.1.33

- Removed extranious items in footer.
- Fixed heading in TOS.

## 2022-07-06 Version 0.1.31 & 0.1.32

- Updated TOS.

## 2022-06-28 Version 0.1.30

- Updated schema.
- Added html support on resource descriptions.

## 2022-06-20 Version 0.1.29

- Facets are now sorted alphabetically.
- Fixed color issue below footer.

## 2022-06-17 version 0.1.28

- Added a favicon.

## 2022-06-17 version 0.1.27

- Updated color theme.

## 2022-06-17 Version 0.1.26

- Added footer between facets that have no items (so maintains consistant spacing).
- Added Industry field and facets.
- Added Sustainability Pillars field and factets.
- Added a Miscellaneous field and facets.
- Updated Wellbeing field.
- Updated Land Use field.
- Reordered Fields.
- Removed tags

## 2022-06-17 Version 0.1.25

- Fixed symboles where an expanded button's symbole is open.

## 2022-06-17 Version 0.1.24

- Fixed symboles for expanding and collapsing.

## 2022-06-16 Version 0.1.23

- Fixed bug with collapsible facets not refreshing open or closed.

## 2022-06-15 Version 0.1.22

- Added collapsible facets.

## 2022-06-14 Version 0.1.21

- Fixed facet bug where labels were used and instead the field value was displayed.

## 2022-06-14 Version 0.1.20

- Updated the logo.

## 2022-06-14 Version 0.1.19

- Removed WWO Attributes field.
- Removed testing field for wellbeing under a multiselect.
- Removed several fields that were not used.
- Added a new field called dimensions.

## 2022-06-14 Version 0.1.18

- Enabled HTML for description fields.
  Not safe if the public were allowed to edit but this is not the case for this project.
- Fixed a minor issue with left over tagging in code.

## 2022-06-10 Version 0.1.17

- Reformatted the home page.

## 2022-06-09 Version 0.1.16

- Removed activity stream and category tags.

## 2022-06-09 Version 0.1.15

- Cleaned up the resource page.

## 2022-06-09 Version 0.1.14

- Added facets from scheming checkboxes.

## 2022-06-03 Version 0.1.13

- Removed Organisation Search Filter (Facet)
- Removed Group Search Filter (Facet)

## 2022-06-03 Version 0.1.12

- Removed Author Search Filter (Facet)
- Removed License Search Filter (Facet)

## 2022-06-03 Version 0.1.11

- Removed Search Widget.

## 2022-06-03 Version 0.1.10

- Updated schema.

## 2022-05-31 Version 0.1.9

- Removed left panel for datasets.

## 2022-05-30 Version 0.1.8

- Removed additional information from datasets.

## 2022-05-30 Version 0.1.7

- Removed hierarchy constraints.

## 2022-05-26 Version 0.1.6

- Removed the social media tags and also the follow display and follow buttons.

## 2022-05-26 Version 0.1.5

- Added Hierarchy extension compatibility.

## 2022-05-26 Version 0.1.4

- Changed the spelling of groups to categories.

## 2022-05-26 Version 0.1.2 & 0.1.3

- Changed spelling of organization to NZ Spelling.

## 2022-05-17 Version 0.1.1

- Fixed description not showing bug.

## 2022-04-21 Version 0.1.0

- Initial Release.
