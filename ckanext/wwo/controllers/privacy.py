from ckan.plugins import toolkit

class PrivacyController(toolkit.BaseController):
    def privacy(self):
        return toolkit.render('privacy.html')
