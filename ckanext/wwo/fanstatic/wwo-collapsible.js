"use strict";

ckan.module('wwo-collapsible', function ($) {
  return {
    initialize: function () {
      $.proxyAll(this, /_on/);
      this.el.on('click', function() {
        this.classList.toggle("is-open");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
          content.style.display = "none";
        } else {
          content.style.display = "block";
	      }
      });
    }
  };
});