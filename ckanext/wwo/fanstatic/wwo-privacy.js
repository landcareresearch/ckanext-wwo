"use strict";

ckan.module('wwo-privacy', function ($) {
  return {

    initialize: function(){
        $.proxyAll(this, /_on/);
        //this.sandbox.client.getTemplate('privacy-popup.html', this.options, this._onReceiveSnippet);

        var sPath = window.location.pathname;
        var sPage = sPath.substring(sPath.lastIndexOf('/') + 1);

        // check if there is a cookie
        var hasAcceptedTOSP = getCookie("hasAcceptedTOSP");

        if (hasAcceptedTOSP == "" ){
          var popup_div = document.getElementById("privacy_popup");
          if ( sPage == "" || sPage == "terms_of_use" || sPage == "privacy" || sPage == "login" || sPage == "register" || sPage == "about" ) {
            popup_div.style.display = "none";
          }else{
            popup_div.style.display = "block";
          }

          // add events
          var checkbox = document.querySelector("input[name=terms_of_use]");
          checkbox.addEventListener('change', checkCheckboxes);

          checkbox = document.querySelector("input[name=privacy]");
          checkbox.addEventListener('change', checkCheckboxes);
        }

        // Toggle for check boxes
        function checkCheckboxes(evt){
          var tosbox          = document.querySelector("input[name=terms_of_use]");
          var privacybox      = document.querySelector("input[name=privacy]");
          var continue_button = document.getElementById("privacyOkButton");
          if (tosbox.checked && privacybox.checked){
            continue_button.disabled = false;
            continue_button.style = "color:black;"
          }else{
            continue_button.disabled = true;
            continue_button.style = "color:darkgrey;"
          }
        };

        var cancelButton = document.getElementById("privacyCancelButton");
        cancelButton.addEventListener("click", function(){
          // go to homepage
          window.location.replace("/");
        });

        var okButton = document.getElementById("privacyOkButton");
        okButton.addEventListener("click", function (){
          setCookie("hasAcceptedTOSP","true",30);

          var popup_div = document.getElementById("privacy_popup");
          popup_div.style.display = "none";
        });


        // get a cookie
        function getCookie(cname) {
          let name = cname + "=";
          let decodedCookie = decodeURIComponent(document.cookie);
          let ca = decodedCookie.split(';');
          for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        };

        // set a cookie for a specified number of days
        function setCookie(cname, cvalue, exdays) {
          const d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          let expires = "expires="+ d.toUTCString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
    },

    // CKAN calls this function when it has rendered the snippet, and passes
    // it the rendered HTML.
    _onReceiveSnippet: function(html) {
    },
  };
});