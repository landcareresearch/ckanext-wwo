"use strict";
ckan.module('wwo_fitness_popup', function ($) {
  return {
    initialize: function () {
      console.log("I've been initialized for element: ", this.el);

      $.proxyAll(this, /_on/);

      this.el.popover({title: this.options.title, html: true,
                       content: this._(
                        "<div class=\"content-info\">"+
                        "<h3>Information on the fitness for purpose table</h3>"+
                        "<ul><li><b>Block / farm</b>:	Land managers, hapū, rural professionals</li>"+
                        "<li><b>Multi-farms (5+)</b>:	Hapū / iwi, Pamu. banks, industry organisations, irrigation companies</li>"+
                        "<li><b>Catchment</b>:	Regional Council (RC) staff, catchment group</li>"+
                        "<li><b>National / regional</b>: RC planner, RC scientist, government analyst, industry analyst</li></ul>"+
                        "<h3>Types of user applications / questions</h3>"+
                        "<h4>Operational: <i>What can you do with this?</i></h4>"+
                        "<ul><li><b>Farm scale example</b>: Changing land use on a farm to this crop</li>"+
			                  "<li><b>Regional scale example</b>: Prohibiting land uses in a particular area</li></ul>"+
                        "<h4>Absolute: <i>How big / significant / bad / much / etc is this?</i></h4>"+
                        "<ul><li><b>Farm scale example</b>: Understanding the environmental impact of this crop in this block</li>"+
			                  "<li><b>Regional scale example</b>: Understanding cumulative impact of land uses and mitigations</li></ul>"+
                        "<h4>Relative: <i>Is this better / smaller / more / etc than that?</i></h4>"+
                        "<ul><li><b>Farm scale example</b>: Understanding relative resilience of land use options</li>"+
			                  "<li><b>Regional scale example</b>: Assessing environmental / economic relativity between land uses / soil types / catchments</li></ul>"+
                        "<h4>Screening (or scoping): <i>Is this interesting / worth considering further?</i></h4>"+
                        "<ul><li><b>Farm scale example</b>: Screening for possible land use options on land block</li>"+
			                  "<li><b>Regional scale example</b>: Identify the priority areas</li>"+
                        "</div>"
                        ), placement: 'right'});
      $('.popover-dismiss').popover({
        trigger: 'focus'
      });

    },
  };
});