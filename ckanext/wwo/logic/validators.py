import sys
import datetime
import math

from paste.util.multidict import MultiDict
from ckan.plugins.toolkit import Invalid

def is_number(value):
    '''Validates value is a number'''
    if value is None:
        return None
    if hasattr(value, 'strip') and not value.strip():
        return None

    try :
        float(value)
    except ValueError :
        raise Invalid("The value is not a number")

    return value

def is_elevation_range(value):
    '''Validates value is within a valid range'''
    if value is None:
        return None
    if hasattr(value, 'strip') and not value.strip():
        return None

    try :
        r = float(value)
        if -20 < r and r < 3000 :
            return value
        else :
            raise Invalid("The value is not a number between -20 and 3000")
    except ValueError :
        raise Invalid("The value is not a number between -20 and 3000")

    return value

def is_year(value):
    '''Validates value is the date format YYYY'''
    try:
        datetime.datetime.strptime(value, '%Y')
    except ValueError:
        raise Invalid("Year must be in the format YYYY, e.g. 2015")
    return value


def is_year_month(value):
    '''Validates value is in the date format YYYY-MM'''
    try:
        datetime.datetime.strptime(value, '%Y-%m')
    except ValueError:
        raise Invalid("Date must be in the format YYYY-MM, e.g. 2015-01")
    return value


def is_year_month_day(value):
    '''Validates value is in the date format YYYY-MM-DD'''
    try:
        datetime.datetime.strptime(value, '%Y-%m-%d')
    except ValueError:
        raise Invalid("Date must be in the format YYYY-MM-DD, e.g. 2015-01-29")
    return value


def is_date(value):
    '''Validates value is in one of three various formats'''
    for m in ['is_year', 'is_year_month', 'is_year_month_day']:
        try:
            getattr(sys.modules[__name__], m)(value)
        except Invalid:
            pass
        else:
            return value

    raise Invalid("Date must be in the format YYYY-MM-DD, YYYY-MM, or YYYY")

# === for creating a bounding box around a lat/lon === #
# Semi-axes of WGS-84 geoidal reference
WGS84_a = 6378137.0  # Major semiaxis [m]
WGS84_b = 6356752.3  # Minor semiaxis [m]

# Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
def WGS84EarthRadius(lat):
    # http://en.wikipedia.org/wiki/Earth_radius
    An = WGS84_a*WGS84_a * math.cos(lat)
    Bn = WGS84_b*WGS84_b * math.sin(lat)
    Ad = WGS84_a * math.cos(lat)
    Bd = WGS84_b * math.sin(lat)
    return math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )

# Bounding box surrounding the point at given coordinates,
# assuming local approximation of Earth surface as a sphere
# of radius given by WGS84
def boundingBox(lat, lon, halfSideHeightInKm, halfSideWidthInKm):
    halfSideH = 1000*halfSideHeightInKm
    halfSideW = 1000*halfSideWidthInKm

    # Radius of Earth at given latitude
    radius = WGS84EarthRadius(lat)
    # Radius of the parallel at given latitude
    pradius = radius*math.cos(lat)

    latMin = lat - halfSideH/radius
    latMax = lat + halfSideH/radius
    lonMin = lon - halfSideW/pradius
    lonMax = lon + halfSideW/pradius

    return (latMin, lonMin, latMax, lonMax)

def convert_spatial(key, data, errors, context):

    # manage empty lat and long fields.
    if data is None:
        return None
    if hasattr(data , 'strip') and not value.strip():
        return None
    if data.get(('latitude',)) is None:
        data[('spatial',)] = ''
        return None
    if data.get(('longitude',)) is None:
        data[('spatial',)] = ''
        return None
    if data.get(('area_height',)) is None:
        area_height = 0.05
    else :
        try:
          area_height = float(data.get(('area_height',)))
        except ValueError:
          raise Invalid("Area Height must be in radians")
    if data.get(('area_width',)) is None:
        area_width = 0.05
    else :
        try:
          area_width = float(data.get(('area_width',)))
        except ValueError:
          raise Invalid("Area Height must be in radians")

    try:
      # get coordinates
      lat = float(data.get(('latitude',)))
      lon = float(data.get(('longitude',)))
    except ValueError:
        raise Invalid("Latitude and Longitude must be in radians")

    # Currently using a 50m area
    bbox = boundingBox(lat,lon,area_height,area_width)

    # store coordinates as lon / lat
    upper_left  = (str(bbox[3]),str(bbox[2]))
    upper_right = (str(bbox[3]),str(bbox[0]))
    lower_right = (str(bbox[1]),str(bbox[0]))
    lower_left  = (str(bbox[1]),str(bbox[2]))

    polygon = "{ \"type\": \"Polygon\",\"coordinates\": [ [ [" + upper_left[0] + ", " + upper_left[1] + "],[" + upper_right[0] + ", " + upper_right[1] + "],[" + lower_right[0] + ", " + lower_right[1] + "], [" + lower_left[0] + ", " + lower_left[1] + "], ["+ upper_left[0] + ", " + upper_left[1] + "] ] ] }"

    # set the spatial value automatically
    data[('spatial',)] = polygon