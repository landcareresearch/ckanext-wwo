import json

from ckan import plugins
from ckan.plugins import toolkit
from routes.mapper import SubMapper, Mapper as _Mapper

import ckanext.wwo.logic.validators as validators
import unicodedata

class WwoPlugin(plugins.SingletonPlugin, toolkit.DefaultDatasetForm):
    plugins.implements(plugins.IRoutes, inherit=True)
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IDatasetForm)
    plugins.implements(plugins.IValidators)
    plugins.implements(plugins.IPackageController, inherit=True)
    plugins.implements(plugins.IFacets)
    plugins.implements(plugins.ITemplateHelpers)
    def get_helpers(self):
        return {
            'wwo_country_codes'       : wwo_country_codes,
            'normalize_strip_accents' : normalize_strip_accents
        }

    def _modify_package_schema(self, schema):
        schema['author'] = [toolkit.get_validator('repeating_text'),
                            toolkit.get_validator('ignore_empty')]
        return schema

    def create_package_schema(self):
        schema = super(WwoPlugin, self).create_package_schema()
        return self._modify_package_schema(schema)

    def update_package_schema(self):
        schema = super(WwoPlugin, self).update_package_schema()
        return self._modify_package_schema(schema)

    def show_package_schema(self):
        schema = super(WwoPlugin, self).show_package_schema()
        schema['author'] = [toolkit.get_validator('repeating_text_output'),
                            toolkit.get_validator('ignore_empty')]
        return schema

    def is_fallback(self):
        return True

    def package_types(self):
        return []

    def before_map(self, map):
        # from the IRoutes plugin, here we add an additional
        # url that maps to our terms of use controller
        terms = 'ckanext.wwo.controllers.terms:TermsController'
        map.connect('terms_of_use', '/terms_of_use',
                    controller=terms, action='terms_of_use')

        privacy = 'ckanext.wwo.controllers.privacy:PrivacyController'
        map.connect('privacy', '/privacy',
                    controller=privacy, action='privacy')

        # replace feeds
        feeds = 'ckanext.wwo.controllers.feed:WwoFeedController'
        map.redirect('/user/', '/user')
        with SubMapper(map, controller=feeds) as m:
             m.connect('/feeds/group/{id}.atom', action='group')
             m.connect('/feeds/organization/{id}.atom', action='organization')
             m.connect('/feeds/tag/{id}.atom', action='tag')
             m.connect('/feeds/dataset.atom', action='general')
             m.connect('/feeds/custom.atom', action='custom')

        return map

    def update_config(self, config):
        # from the IConfigurer interface, we're telling ckan
        # where our templates are kept in this pluign
        toolkit.add_template_directory(config, 'templates')

        # add our extension's public directory, to include the custom css file
        toolkit.add_public_directory(config, 'public')

        toolkit.add_resource('fanstatic', 'wwo')

    def get_validators(self):
        return {
            'ckanext_wwo_is_year'            : validators.is_year,
            'ckanext_wwo_is_date'            : validators.is_date,
            'ckanext_wwo_is_number'          : validators.is_number,
            'ckanext_wwo_is_elevation_range' : validators.is_elevation_range,
            'ckanext_wwo_convert_spatial'    : validators.convert_spatial,
        }

    def before_index(self, dataset_dict):
        dataset_dict['industry']         = json.loads(dataset_dict.get('industry',         '[]'))
        dataset_dict['landuse']          = json.loads(dataset_dict.get('landuse',          '[]'))
        dataset_dict['sustainability']   = json.loads(dataset_dict.get('sustainability',   '[]'))
        dataset_dict['miscellaneous']    = json.loads(dataset_dict.get('miscellaneous',    '[]'))
        return dataset_dict

    # IFacets

    def dataset_facets(self, facets_dict, package_type):
        facets_dict.pop('organization')
        facets_dict.pop('groups'      )
        facets_dict.pop('license_id'  )
        facets_dict.pop('tags'        )
        facets_dict.pop('res_format'  )
        facets_dict['industry'        ] = plugins.toolkit._('Industry'               )
        facets_dict['landuse'         ] = plugins.toolkit._('Land Use'               )
        facets_dict['sustainability'  ] = plugins.toolkit._('Sustainability Pillars' )
        facets_dict['miscellaneous'   ] = plugins.toolkit._('Miscellaneous'          )

        facets_dict.update({
            'tags'       : toolkit._('Tags'),
            'res_format' : toolkit._('Formats'),
        })

        return facets_dict

    def group_facets(self, facets_dict, group_type, package_type):
        _update_facets(facets_dict)
        return facets_dict

    def organization_facets(self, facets_dict, organization_type, package_type):
        _update_facets(facets_dict)
        return facets_dict

def _update_facets(facets_dict):

    facets_dict.update({
        'industry'         : plugins.toolkit._('Industry'               ),
        'landuse'          : plugins.toolkit._('Land Use'               ),
        'sustainability'   : plugins.toolkit._('Sustainability Pillars' ),
        'miscellaneous'    : plugins.toolkit._('Miscellaneous'          ),
    })

def create_country_codes():
    user = toolkit.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'country_codes'}
        toolkit.get_action('vocabulary_show')(context, data)
    except toolkit.ObjectNotFound:
        data = {'name': 'country_codes'}
        vocab = toolkit.get_action('vocabulary_create')(context, data)
        for tag in (u'uk', u'ie', u'de', u'fr', u'es'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            toolkit.get_action('tag_create')(context, data)

def wwo_country_codes(field):
    create_country_codes()
    try:
        tag_list = toolkit.get_action('tag_list')
        country_codes = tag_list(data_dict={'vocabulary_id': 'country_codes'})

#choices:
#- value: bactrian
#  label: Bactrian Camel
#- value: hybrid
#  label: Hybrid Camel


        return country_codes
    except toolkit.ObjectNotFound:
        return None

def normalize_strip_accents(s):
    """
    utility function to help with sorting our French strings
    """
    if isinstance(s, str):
        return s
    if not s:
        s = u''
    s = unicodedata.normalize('NFD', s)
    return s.encode('ascii', 'ignore').decode('ascii').lower()
