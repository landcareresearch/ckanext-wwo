module MyVars
    HOSTNAME               = "local-wwo"
    OS_FILE                = "ubuntu18.04"
    MEMORY                 = "4096"
    CPUS                   = "1"
    IP                     = "192.168.56.25"
    GENCACHE               = false
    PROJECT_PATH           = "../"
    FOLDERS                = {
        "ckanext-wwo"      => {
            "src"  => "../.",
            "dest" => "/opt/ckanext-wwo"
        }
    }
end
