module MyCredentials
    PUPPET_CONTROL_DIR     = "/home/spethm/Work/Landcare/dev/puppet-control"
    PUPPET_CONTROL_DEV_DIR = "/home/spethm/Work/Landcare/dev/puppet-control-devs/puppet-control-dev-root"
    PUPPET_REPO_DIR        = "/home/spethm/Work/Landcare/dev/admin-puppet-repo"
    HIERA_KEY_PATH         = "/home/spethm/Work/Landcare/admin/private/hiera/keys"
end
