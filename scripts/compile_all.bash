#!/bin/bash
function setup {
  pushd .
  sudo ckan_activate_exec.bash pip install $i
  cd $i
  sudo ckan_activate_exec.bash python setup.py install
  sudo ckan_activate_exec.bash python setup.py develop
  popd
}

dirs=$(ls -d ckanext*)
for i in $dirs ; do
  echo $i
  setup $i
done
